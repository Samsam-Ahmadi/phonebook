import "core/firebase/firebase";

import { Header } from "layout/header/Header";
import { Layout } from "layout";
import { Login } from "pages";
import { Providers } from "core";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

function App() {
  return (
    <Router>
      <Providers>
        <Header />
        <Switch>
          <Route path="/login" component={Login} />
          <Route path="/" component={Layout} />
        </Switch>
      </Providers>
    </Router>
  );
}

export default App;
