const baseUrl = process.env.REACT_APP_API;

export const AUTHENTICATION_API = {
  LOGIN: baseUrl + "/auth/login",
  ME: baseUrl + "/auth/me",
};

export const CONTACT_API = {
  CREATE: baseUrl + "/contacts",
};
