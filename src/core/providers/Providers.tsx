import { CssBaseline } from "@material-ui/core";
import { FC } from "react";
import { SnackbarProvider } from "notistack";

import { GlobalProvider } from "./global/global.provider";
import { ThemeProvider } from "./theme/Theme.provider";

export const Providers: FC = ({ children }) => {
  return (
    <GlobalProvider>
      <ThemeProvider>
        <CssBaseline />
        <SnackbarProvider
          autoHideDuration={3000}
          hideIconVariant={false}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "center",
          }}
        >
          {children}
        </SnackbarProvider>
      </ThemeProvider>
    </GlobalProvider>
  );
};
