import React, { createContext, useContext, useState } from "react";
import { AUTHENTICATION_API } from "constants/apiRoutes";
import { MeResponse, User } from "shared/interfaces/authentication";
import { useFetch } from "shared/hooks";
import { useHistory } from "react-router-dom";

export const GlobalContext = createContext<ContextType | null>(null);

export const GlobalProvider: React.FC = ({ children }) => {
  const [user, setUser] = useState<User | null>(null);
  const history = useHistory();

  const { data, loading } = useFetch<MeResponse>(AUTHENTICATION_API.ME);

  if (history.location.pathname.includes("/login") && !loading && data?.id) {
    history.push("/");
  }

  return (
    <GlobalContext.Provider
      value={{ loading: data?.id ? false : loading, user: user || data, setUser }}
    >
      {children}
    </GlobalContext.Provider>
  );
};

export const useGlobal = () => {
  const global = useContext(GlobalContext);

  if (!global) {
    throw new Error("useGlobal must be inside in GlobalProvider");
  }

  return global;
};
interface ContextType {
  loading: boolean;
  user: User | null;
  setUser: React.Dispatch<React.SetStateAction<User | null>>;
}
