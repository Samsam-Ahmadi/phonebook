import React from "react";
import { ThemeProvider as ThemeProviderMaterial, createMuiTheme } from "@material-ui/core";

import theme from "./theme";

export const ThemeProvider: React.FC = ({ children }) => {
  return <ThemeProviderMaterial theme={createMuiTheme(theme)}>{children}</ThemeProviderMaterial>;
};
