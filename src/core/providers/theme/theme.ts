import { ThemeOptions } from "@material-ui/core";

const palette: NonNullable<ThemeOptions["palette"]> = {
  primary: {
    light: "#7FCAAD",
    main: "#00955C",
    dark: "#00955C",
    contrastText: "#fff",
  },
  secondary: {
    main: "#E5FAF2",
  },
  background: {
    paper: "#fff",
    default: "#F8FFFC",
  },
  text: {
    primary: "#1B1B1D",
    secondary: "#47464C",
    disabled: "#BABABA",
    hint: "#707070",
  },
  action: {
    hover: "inherit",
  },
};

const theme: ThemeOptions = {
  overrides: {
    MuiTableCell: {
      body: {
        padding: "11px ​16px",
      },
    },
    MuiButton: {
      contained: {
        backgroundColor: "#00955C",
        padding: "16px 22px",
        color: "#FFF",
        boxShadow: "unset",
        "&:hover": {
          backgroundColor: "#00955C",
          boxShadow: "unset",
        },
        "&:active": {
          boxShadow: "unset",
        },
        "&:disabled": {
          backgroundColor: palette.text?.disabled,
          color: palette.background?.paper,
        },
      },
      label: {
        textTransform: "none",
      },
      containedSecondary: {
        backgroundColor: "#E5FAF2",
        "&:hover": {
          boxShadow: "unset",
          backgroundColor: "#E5FAF2",
        },
        "&:active": {
          boxShadow: "unset",
        },
      },
      containedPrimary: {
        backgroundColor: "#00955C",
        "&:hover": {
          boxShadow: "unset",
          backgroundColor: "#00955C",
        },
        "&:active": {
          boxShadow: "unset",
        },
      },
      outlined: {
        padding: "15px 22px",
        backgroundColor: "#ffffff",
        color: "#7E7E7E",
        border: "1px solid #BFBFBF",
        "&:hover": {
          backgroundColor: "inherit",
        },
        "&:active": {
          boxShadow: "unset",
        },
      },
      outlinedPrimary: {
        backgroundColor: "#ffffff",
        color: "#00955C",
        border: "1px solid #00955C",
        "&:hover": {
          backgroundColor: "inherit",
        },
        "&:active": {
          boxShadow: "unset",
        },
      },
    },
    MuiOutlinedInput: {
      root: {
        "& $notchedOutline": {
          borderColor: "#00955C",
        },
        "&:hover $notchedOutline": {
          borderColor: "#00955C ",
        },
        "&$focused $notchedOutline": {
          boxShadow: "unset",
          borderWidth: "1px",
          borderColor: "#00955C",
        },
      },
    },
    MuiFormLabel: {
      root: {
        "&$focused": {
          color: palette.grey?.[800],
        },
      },
    },
    MuiInputBase: {
      input: {
        "&::placeholder": {
          color: "#00955C",
        },
      },
      root: {
        color: "#222",
        backgroundColor: "transparent",
        "&.Mui-disabled": {
          backgroundColor: "#f5f5f5",
          color: "#9e9e9e",
        },
      },
    },
    MuiFormHelperText: {
      contained: {
        marginRight: 0,
        marginLeft: 0,
      },
      root: {
        color: "#616161",
      },
    },
  },
  palette,
  shape: {
    borderRadius: 6,
  },
};
export default theme;
