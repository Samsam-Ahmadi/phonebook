import { Routes } from "./Routes";

export const Layout = () => {
  return (
    <main>
      <Routes />
    </main>
  );
};
