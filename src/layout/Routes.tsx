import { Box, CircularProgress } from "@material-ui/core";
import { Contacts } from "pages";
import { Redirect, Route, Switch } from "react-router";
import { useGlobal } from "core/providers/global/global.provider";

import { NotFound } from "./notFount/NotFound";

export const Routes = () => {
  const { user, loading } = useGlobal();

  if (loading)
    return (
      <Box display="flex" justifyContent="center" alignItems="center" height="100vh" width="100%">
        <CircularProgress />
      </Box>
    );
  if (!loading && !user) return <Redirect to="/login" />;

  return (
    <Switch>
      <Route exact path={"/"} component={Contacts} />
      <Route path="*" component={NotFound} />
    </Switch>
  );
};
