import { Grid, Link, Typography, makeStyles } from "@material-ui/core";
import { useGlobal } from "core/providers/global/global.provider";
import { useHistory } from "react-router";

export const Header = () => {
  const styles = useStyles();
  const { user, setUser } = useGlobal();
  const history = useHistory();
  const logout = () => {
    localStorage.removeItem("token");
    setUser(null);
    history.push("/login");
  };

  return (
    <header className={styles.root}>
      <nav>
        <Grid container className={styles.wrapper}>
          <Grid item xs={6} container alignItems="center">
            <Typography component={Link} href="/" variant="h4" color="primary">
              Phonebook
            </Typography>
          </Grid>
          <Grid item xs={6} container justify="flex-end" alignItems="center">
            <Link href="/login" onClick={logout}>
              {user?.id ? "Logout" : "Login"}
            </Link>
          </Grid>
        </Grid>
      </nav>
    </header>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    height: "60px",
    width: "100%;",
    padding: "0px 30px",
    backgroundColor: theme.palette.background.paper,
    boxShadow: "0px 3px 6px #4EB99029",
    "& a:hover": {
      textDecoration: "none",
    },
  },
  wrapper: {
    height: "60px",
  },
}));
