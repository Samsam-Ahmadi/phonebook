import DeleteForeverRoundedIcon from "@material-ui/icons/DeleteForeverRounded";
import EditRoundedIcon from "@material-ui/icons/EditRounded";
import {
  Avatar,
  Grid,
  IconButton,
  TableCell,
  TableRow,
  Typography,
  makeStyles,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import { Contact } from "shared/interfaces/contacts";
import { FC } from "react";

type Props = {
  item: Contact;
};
export const ContactItem: FC<Props> = ({ item }) => {
  const {
    breakpoints: { down },
  } = useTheme();
  const isMobile = useMediaQuery(down("md"));
  const fullName = item?.firstName + " " + item?.lastName;
  const classes = useStyles();
  return (
    <TableRow className={classes.root}>
      <TableCell component="th" scope="row">
        <Grid container alignItems="center" direction="row" wrap="nowrap">
          <Avatar className={classes.avatar}>
            <span>{(fullName + "S").charAt(0).toLocaleUpperCase()}</span>
          </Avatar>
          <Grid item container direction="column">
            <Typography>{fullName}</Typography>
            {isMobile && <Typography>{item.phoneNumber}</Typography>}
          </Grid>
        </Grid>
      </TableCell>

      {!isMobile && (
        <>
          <TableCell component="th" scope="row">
            <Typography>{item.phoneNumber}</Typography>
          </TableCell>
          <TableCell component="th" scope="row">
            {item.address}
          </TableCell>
          <TableCell align="right" component="th" scope="row">
            <IconButton aria-label="close dialog">
              <EditRoundedIcon aria-label="edit contact" />
            </IconButton>
            <IconButton aria-label="delete contact">
              <DeleteForeverRoundedIcon />
            </IconButton>
          </TableCell>
        </>
      )}
    </TableRow>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: "#FFF",
    transition: "all .3s",
    "&:hover": {
      backgroundColor: "#E5FAF2",
      "& th": {
        "&:last-child svg": {
          transition: "all .3s",
          visibility: "visible",
        },
      },
    },
    "& th": {
      color: theme.palette.primary.main,
      "& button": {
        color: theme.palette.primary.main,
      },
      "&:last-child svg": {
        visibility: "hidden",
      },
    },
  },
  avatar: {
    background: theme.palette.primary.main,
    width: 40,
    height: 40,
    marginRight: 20,
    "& span": {
      position: "relative",
      bottom: 5,
    },
  },
}));
