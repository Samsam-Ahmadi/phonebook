import {
  Button,
  Card,
  CardContent,
  Container,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  makeStyles,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import { Contact } from "shared/interfaces/contacts";
import { firestore } from "core";
import { useEffect, useState } from "react";

import { ContactItem } from "./ContactItem";
import { ContractItemLoading } from "./ContractItemLoading";
import { CreateContact } from "./CreateContact";

export const Contacts = () => {
  const [loading, setLoading] = useState(true);
  const {
    breakpoints: { down },
  } = useTheme();
  const isMobile = useMediaQuery(down("md"));
  const [contacts, setContacts] = useState<Contact[]>([]);

  const classes = useStyles();
  useEffect(() => {
    firestore.collection("Contact").onSnapshot(function (snapshot) {
      const contact: Contact[] = [];
      snapshot.docs.forEach(function (item) {
        if (item.exists) {
          const data = item.data() as Contact;
          contact.push(data);
        }
      });
      setContacts(contact);
      setLoading(false);
    });
  }, []);
  return (
    <Container className={classes.root}>
      <Grid container direction="row" spacing={3}>
        <Grid container item xs={12} md={4} lg={3} spacing={1} direction="column">
          <Grid item>
            <CreateContact />
          </Grid>
          {!isMobile && (
            <Grid item>
              <Button fullWidth color="secondary" variant="contained">
                Contacts
              </Button>
            </Grid>
          )}
        </Grid>
        <Grid item xs={12} md={8} lg={9}>
          <Card variant="outlined">
            <CardContent>
              <Table className={classes.table} aria-label="contacts table">
                {!isMobile && (
                  <TableHead>
                    <TableRow>
                      <TableCell>Name</TableCell>
                      <TableCell>Phone number</TableCell>
                      <TableCell>Email address</TableCell>
                      <TableCell></TableCell>
                    </TableRow>
                  </TableHead>
                )}
                <TableBody>
                  {loading && <ContractItemLoading />}
                  {/* I used the index like key for this part because we don't have any specific id's */}
                  {contacts.map((item, index) => (
                    <ContactItem key={index} item={item} />
                  ))}
                </TableBody>
              </Table>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Container>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    marginTop: 60,
    [theme.breakpoints.down("md")]: {
      marginTop: 0,
    },
  },
  table: {
    "& tbody tr th": {
      borderBottom: 0,
    },
    "& thead th": {
      color: theme.palette.primary.main,
      fontWeight: "bold",
    },
  },
}));
