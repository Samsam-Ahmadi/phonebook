import Skeleton from "@material-ui/lab/Skeleton";
import { FC } from "react";
import { Grid, TableCell, TableRow, makeStyles, useMediaQuery, useTheme } from "@material-ui/core";

export const ContractItemLoading: FC = () => {
  const {
    breakpoints: { down },
  } = useTheme();
  const isMobile = useMediaQuery(down("md"));
  const classes = useStyles();
  const Item = ({ opacity }: { opacity: number }) => (
    <TableRow className={classes.root} style={{ opacity: opacity }}>
      <TableCell component="th" scope="row">
        <Grid container alignItems="center" direction="row" wrap="nowrap">
          <Skeleton variant="circle" width={40} height={40} style={{ marginRight: 20 }} />

          <Grid item container direction="column">
            <Skeleton variant="text" width={100} />
            {isMobile && <Skeleton variant="text" width={60} />}
          </Grid>
        </Grid>
      </TableCell>

      {!isMobile && (
        <>
          <TableCell component="th" scope="row">
            <Skeleton variant="text" width={150} />
          </TableCell>
          <TableCell component="th" scope="row">
            <Skeleton variant="text" width={150} />
          </TableCell>
          <TableCell align="right" component="th" scope="row"></TableCell>
        </>
      )}
    </TableRow>
  );

  return (
    <>
      <Item opacity={1} />
      <Item opacity={1} />
      <Item opacity={0.8} />
      <Item opacity={0.5} />
      <Item opacity={0.2} />
      <Item opacity={0.2} />
    </>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: "#FFF",
    transition: "all .3s",
    "&:hover": {
      backgroundColor: "#E5FAF2",
    },
    "& th": {
      color: theme.palette.primary.main,
    },
  },
}));
