import AddIcon from "@material-ui/icons/Add";
import CloseIcon from "@material-ui/icons/Close";
import {
  Avatar,
  Box,
  Button,
  Container,
  Dialog,
  Fab,
  Grid,
  IconButton,
  TextField,
  makeStyles,
  useMediaQuery,
  useTheme,
} from "@material-ui/core";
import { CONTACT_API } from "constants/apiRoutes";
import { useFetch, useToast } from "shared/hooks";
import { useForm } from "react-hook-form";
import { useGlobal } from "core/providers/global/global.provider";
import { useState } from "react";
import { yupResolver } from "@hookform/resolvers/yup";

import { createContactSchema } from "./create.schema";

type FormInput = {
  firstName: string;
  lastName: string;
  phoneNumber: string;
  address: string;
  userId: string;
};
export const CreateContact = () => {
  const { user } = useGlobal();
  const [open, setOpen] = useState(false);
  const { toast } = useToast();
  const {
    breakpoints: { down },
  } = useTheme();
  const isMobile = useMediaQuery(down("md"));

  const { handleSubmit, formState, register, getValues, watch } = useForm<FormInput>({
    resolver: yupResolver(createContactSchema),
    defaultValues: {
      userId: user?.id,
    },
    mode: "all",
  });
  const { call, loading } = useFetch(CONTACT_API.CREATE, {
    method: "post",
    defaultLoading: false,
    body: JSON.stringify(getValues()),
  });

  const onFormSubmit = async () => {
    await call();
    toast("Contact created  successfully", { variant: "success" });
    setOpen(false);
  };

  const classes = useStyles();
  return (
    <>
      {!isMobile && (
        <Button
          aria-label="Create contact"
          fullWidth
          color="primary"
          variant="contained"
          onClick={() => setOpen(true)}
        >
          + Create contact
        </Button>
      )}
      {isMobile && (
        <Fab
          color="primary"
          aria-label="add contact"
          className={classes.fab}
          onClick={() => setOpen(true)}
        >
          <AddIcon />
        </Fab>
      )}
      <Dialog
        open={open}
        onClose={() => setOpen(false)}
        aria-label="Dialog of creation contract is open now"
        fullScreen={isMobile}
      >
        {isMobile && (
          <Box pl={2} pt={2}>
            <IconButton aria-label="close dialog" onClick={() => setOpen(false)}>
              <CloseIcon fontSize="small" />
            </IconButton>
          </Box>
        )}
        <Container className={classes.root} component="form" onSubmit={handleSubmit(onFormSubmit)}>
          <Grid container direction="column" spacing={2}>
            <Grid item container justify={isMobile ? "center" : "flex-start"}>
              <Avatar className={classes.avatar}>
                <span>
                  {(watch("firstName") + watch("lastName") + "S").charAt(0).toLocaleUpperCase()}
                </span>
              </Avatar>
            </Grid>
            <Grid item container spacing={2}>
              <Grid item xs={6}>
                <TextField
                  variant="outlined"
                  placeholder="First name"
                  id="firstName"
                  type="firstName"
                  {...register("firstName")}
                  error={formState.touchedFields.firstName && !!formState.errors.firstName?.message}
                  helperText={
                    formState.touchedFields.firstName && formState.errors.firstName?.message
                  }
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  variant="outlined"
                  placeholder="Last name"
                  id="lastName"
                  type="lastName"
                  {...register("lastName")}
                  error={formState.touchedFields.lastName && !!formState.errors.lastName?.message}
                  helperText={
                    formState.touchedFields.lastName && formState.errors.lastName?.message
                  }
                />
              </Grid>
            </Grid>
            <Grid item>
              <TextField
                fullWidth
                variant="outlined"
                placeholder="Phone number"
                id="phoneNumber"
                type="phoneNumber"
                {...register("phoneNumber")}
                error={
                  formState.touchedFields.phoneNumber && !!formState.errors.phoneNumber?.message
                }
                helperText={
                  formState.touchedFields.phoneNumber && formState.errors.phoneNumber?.message
                }
              />
            </Grid>
            <Grid item>
              <TextField
                fullWidth
                variant="outlined"
                placeholder="Email address"
                id="address"
                type="address"
                {...register("address")}
                error={formState.touchedFields.address && !!formState.errors.address?.message}
                helperText={formState.touchedFields.address && formState.errors.address?.message}
              />
            </Grid>
            <Grid item container spacing={2} direction="column">
              <Grid item>
                <Button
                  disabled={loading || !formState.isValid}
                  fullWidth
                  color="primary"
                  variant="contained"
                  type="submit"
                >
                  Create
                </Button>
              </Grid>
              <Grid item>
                <Button fullWidth color="default" variant="outlined" onClick={() => setOpen(false)}>
                  Cancel
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Container>
      </Dialog>
    </>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    padding: 30,
  },
  fab: {
    position: "fixed",
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
  avatar: {
    background: theme.palette.primary.main,
    width: 80,
    height: 80,
    marginRight: 20,
    "& span": {
      position: "relative",
      bottom: 5,
      fontSize: 40,
    },
  },
}));
