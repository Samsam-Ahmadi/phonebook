import * as yup from "yup";

export const createContactSchema = yup.object().shape({
  firstName: yup.string().required("FirstName is required"),
  lastName: yup.string().required("LastName is required"),
  phoneNumber: yup.string().required("PhoneNumber is required"),
  address: yup.string().email("Please enter a valid email").required("Address is required"),
  userId: yup.string().required("UserId is required"),
});
