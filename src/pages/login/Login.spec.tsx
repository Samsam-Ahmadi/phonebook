import { TestProvider } from "shared/components/TestProviders";
import {
  fireEvent,
  render,
  screen,
  waitFor,
  waitForElementToBeRemoved,
} from "@testing-library/react";

beforeAll(() => jest.spyOn(window, "fetch"));
jest.spyOn(window.localStorage.__proto__, "setItem");
window.localStorage.__proto__.setItem = jest.fn();

describe("Page - Login - check fields", () => {
  it("should have email and password fields", async () => {
    await renderLogin();
    const login = screen.getByLabelText(/Enter email address/);
    const password = screen.getByLabelText(/Enter password/);
    const loginButton = screen.getByLabelText("Login button");

    expect(login).toBeDefined();
    expect(password).toBeDefined();
    expect(loginButton).toBeDefined();

    await waitFor(() => expect(loginButton).toBeDisabled());
  });
});

describe("Page - Login - check validation", () => {
  it("should have email validation work correctly", async () => {
    await renderLogin();
    const login = screen.getByLabelText("Enter email address").querySelector("input")!;
    const password = screen.getByLabelText("Enter password").querySelector("input")!;

    const emailErrorRequire = () => screen.getByText("Email is required");
    const emailErrorInvalid = () => screen.getByText("Please enter a valid email");

    fireEvent.blur(login);
    fireEvent.click(login);
    fireEvent.click(password);

    await waitFor(() => expect(emailErrorRequire()).toBeInTheDocument());
    fireEvent.change(login, { target: { value: "test" } });

    await waitFor(() => expect(emailErrorInvalid()).toBeInTheDocument());
    fireEvent.change(login, { target: { value: "test@test.com" } });
    await waitForElementToBeRemoved(() => emailErrorInvalid());
  });

  it("should have password validation work correctly", async () => {
    await renderLogin();
    const login = screen.getByLabelText("Enter email address").querySelector("input")!;
    const password = screen.getByLabelText("Enter password").querySelector("input")!;

    const passwordErrorRequire = () => screen.queryByText("Password is required");

    fireEvent.click(password);
    fireEvent.blur(password);
    fireEvent.click(login);

    await waitFor(() => expect(passwordErrorRequire()).toBeInTheDocument());

    fireEvent.change(password, { target: { value: "test password" } });
    await waitForElementToBeRemoved(() => passwordErrorRequire());
  });

  it("should enabled button after valid values", async () => {
    await renderLogin();
    const login = screen.getByLabelText("Enter email address").querySelector("input")!;
    const password = screen.getByLabelText("Enter password").querySelector("input")!;
    fireEvent.click(login);
    fireEvent.change(login, { target: { value: "test@test.com" } });
    fireEvent.click(password);
    fireEvent.change(password, { target: { value: "test password" } });
    const loginButton = screen.getByLabelText(/Login button/);
    await waitFor(() => expect(loginButton).toBeEnabled());
  });
});

const renderLogin = async () => {
  const { Login } = require("./Login");
  return render(
    <TestProvider>
      <Login />
    </TestProvider>
  );
};
