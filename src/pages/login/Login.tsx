import { AUTHENTICATION_API } from "constants/apiRoutes";
import {
  Button,
  Container,
  Grid,
  Paper,
  TextField,
  Typography,
  makeStyles,
} from "@material-ui/core";
import { LoginResponse } from "shared/interfaces/authentication";
import { useForm } from "react-hook-form";
import { useGlobal } from "core/providers/global/global.provider";
import { useHistory } from "react-router";
import { useState } from "react";
import { useToast } from "shared/hooks";
import { yupResolver } from "@hookform/resolvers/yup";

import { loginSchema } from "./login.schema";

type FormInput = {
  email: string;
  password: string;
};
export const Login = () => {
  const history = useHistory();
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const { setUser } = useGlobal();
  const { toast } = useToast();
  const { handleSubmit, formState, register, watch } = useForm<FormInput>({
    resolver: yupResolver(loginSchema),
    mode: "all",
  });

  const onFormSubmit = async () => {
    setLoading(true);
    try {
      const res = await fetch(AUTHENTICATION_API.LOGIN, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: watch("email"),
          password: watch("password"),
        }),
      });
      const result: LoginResponse = await res.json();
      localStorage.setItem("token", result.token.accessToken);
      localStorage.setItem("tokenType", result.token.tokenType);
      setUser(result.user);
      setLoading(false);
      history.push("/");
      toast("Login was success", { variant: "success" });
    } catch (error) {
      toast(error, { variant: "error" });
      console.log("🚀 ~ file: Login.tsx ~ line 47 ~ onFormSubmit ~ error", error);
    }
  };
  return (
    <Container>
      <Grid container item justify="center" alignItems="center" className={classes.root}>
        <Paper component="form" className={classes.wrapper} onSubmit={handleSubmit(onFormSubmit)}>
          <Typography color="primary" variant="h4" align="center" className={classes.title}>
            Welcome to Phonebook!
          </Typography>
          <Grid container direction="column" spacing={3}>
            <Grid item container direction="column">
              <Typography color="primary" variant="h6" className={classes.label}>
                Email address
              </Typography>
              <TextField
                fullWidth
                variant="outlined"
                placeholder="Enter email address"
                aria-label="Enter email address"
                id="email"
                type="email"
                {...register("email")}
                error={formState.touchedFields.email && !!formState.errors.email?.message}
                helperText={formState.touchedFields.email && formState.errors.email?.message}
              />
            </Grid>
            <Grid item container direction="column">
              <Typography color="primary" variant="h6" className={classes.label}>
                Password
              </Typography>
              <TextField
                fullWidth
                variant="outlined"
                placeholder="Enter password"
                aria-label="Enter password"
                id="password"
                type="password"
                {...register("password")}
                error={formState.touchedFields.password && !!formState.errors.password?.message}
                helperText={formState.touchedFields.password && formState.errors.password?.message}
              />
            </Grid>
            <Grid item container direction="column">
              <Button
                fullWidth
                type="submit"
                aria-label="Login button"
                variant="contained"
                disabled={!formState.isValid || loading}
              >
                Login
              </Button>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
    </Container>
  );
};

const useStyles = makeStyles(theme => ({
  root: {
    height: theme.breakpoints.down("md") ? "calc(100vh - 70px)" : "100vh",
  },
  title: {
    marginBottom: 50,
  },
  wrapper: {
    maxWidth: "560px",
    width: "100%",
    boxShadow: "unset",
    border: `1px solid ${theme.palette.divider}`,
    padding: theme.spacing(5),
  },
  label: {
    marginBottom: 15,
  },
}));
