import { GlobalContext } from "core/providers/global/global.provider";
import { SnackbarProvider } from "notistack";
import { User } from "shared/interfaces/authentication";
import { useState } from "react";

export const TestProvider: React.FC = ({ children }) => {
  const [user, setUser] = useState<User | null>(null);
  return (
    <GlobalContext.Provider value={{ loading: false, user, setUser }}>
      <SnackbarProvider
        autoHideDuration={3000}
        hideIconVariant={false}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
      >
        {children}
      </SnackbarProvider>
    </GlobalContext.Provider>
  );
};
