import { useCallback, useEffect, useState } from "react";
import { useHistory } from "react-router";
interface Props extends RequestInit {
  defaultLoading?: boolean;
}
export function useFetch<T>(url: string, configProps: Props = { defaultLoading: true }) {
  const { defaultLoading = true, ...fetchConfig } = configProps;

  const [loading, setLoading] = useState(defaultLoading);
  const [hasError, setHasError] = useState(false);
  const [data, setData] = useState<T | null>(null);
  const history = useHistory();

  const getData = useCallback(async () => {
    setLoading(true);
    const config: RequestInit = {
      ...fetchConfig,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    };
    try {
      const res = await fetch(url, config);
      const result = await res.json();
      if (result.code === 401) {
        localStorage.removeItem("token");
        history.push("/login");
        setHasError(true);
      } else {
        setData(result);
      }
    } catch (error) {
      setHasError(true);
    }
    setLoading(false);
  }, [url, history, fetchConfig]);

  useEffect(() => {
    if (!configProps?.method?.includes("post")) {
      getData();
    }
    // eslint-disable-next-line
  }, []);

  return { data, hasError, loading, call: getData };
}
