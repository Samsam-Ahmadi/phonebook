import { useSnackbar } from "notistack";

const useToast = () => {
  const { enqueueSnackbar: toast, ...other } = useSnackbar();
  if (!global) {
    throw new Error("useToast must be inside in SnackbarProvider");
  }
  return { toast, other };
};

export { useToast };
