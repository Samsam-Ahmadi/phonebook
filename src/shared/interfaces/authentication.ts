export interface User {
  createdAt: string;
  email: string;
  id: string;
  isBlocked: string;
  isEmailVerified: string;
  password: string;
  role: string;
  updatedAt: string;
}

export interface LoginResponse {
  token: {
    accessToken: string;
    expiresIn: string;
    refreshToken: string;
    tokenType: string;
  };
  user: User;
}

export interface MeResponse extends User {}
